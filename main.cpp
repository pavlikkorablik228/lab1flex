#include <iostream>
#include <vector>
#include "Aim.h"
#include <FlexLexer.h>
#include "Container.h"

int getInt(int* a) {
	int n = 0;
	do {
		n = scanf("%d", a);
		if (n == 0) {
			printf("Error! Type integer again\n>> ");
			scanf("%*[^\n]");
		}
	} while (n == 0);
	scanf("%*c");
	return n < 0 ? 1 : 0;
}

int main() {
	yyFlexLexer ftp;
    int amount;
	int lexoutput;
	Container::index = -1;
	Container::result.clear();
    std::cout << "Please enter the amount of strings:" << std::endl << ">> ";
    while (getInt(&amount)) std::cout << "Incorrect amount of strings!!! Please try again!" << std::endl << ">> ";
	while (amount-- > 0) {
		lexoutput = ftp.yylex();
	}
	auto result = Container::getVec();
	for (auto it = result.begin(); it != result.end(); it++) (*it).showFull();
    return 0;
}