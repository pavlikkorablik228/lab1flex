%option noyywrap
%option c++
%x REQAIMS
%{
    #include "Aim.h"
    #include "Container.h"
    #include <iostream>
    #include <vector>
%}

AIM     ([a-z]|\.|\_)([a-z0-9]|\.|\_)*
SPACE   [ \t]*

%%
<INITIAL>{
    ^(?i:{AIM})/(?i:{SPACE}":"({SPACE}{AIM}{SPACE})*\n)   {Container::addAim(YYText());BEGIN(REQAIMS);}
    \n          {return 0;}
    <<EOF>>     {return -1;}
    .           {}
}

<REQAIMS>{
    {SPACE}     {}
    ":"         {}
    {AIM}       {Container::addReqAim(YYText());}
    \n          {BEGIN(INITIAL);return 0;}
    <<EOF>>     {return -1;}
    .           {Container::delAim();BEGIN(INITIAL);}
}

%%