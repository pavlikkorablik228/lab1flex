#include "Aim.h"
#include <vector>

class Container {
public:
    static std::vector<Aim> result;
    static int index;
    Container() {};
    ~Container();
    static void addAim(std::string);
    static void delAim();
    static void addReqAim(std::string);
    static std::vector<Aim> getVec();
};