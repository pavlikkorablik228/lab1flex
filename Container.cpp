#include "Container.h"

std::vector<Aim> Container::result;
int Container::index;

void Container::addAim(std::string str) {
    Aim aim(str);
    result.push_back(aim);
    index++;
}

void Container::delAim() {
    result.pop_back();
    index--;
}

void Container::addReqAim(std::string str) {
    result[index].addReq(str);
}

std::vector<Aim> Container::getVec() {return result;}

